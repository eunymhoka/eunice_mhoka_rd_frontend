import 'react-native-gesture-handler';

import React, { Component } from 'react';
import { StyleSheet } from 'react-native'; 

import { createStackNavigator } from '@react-navigation/stack'; 
import { NavigationContainer } from '@react-navigation/native'; 

import ComputedMarks from './pages/ComputedMarks';
import HistoricalCalculations from './pages/HistoricalCalculations';
import Home from './pages/Home';

const Stack = createStackNavigator();
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator> 
            <Stack.Screen
            name="Home"
            component={Home}
          /> 
          <Stack.Screen
            name="Computed Marks"
            component={ComputedMarks}
          />
            <Stack.Screen
            name="History"
            component={HistoricalCalculations}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
     backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;