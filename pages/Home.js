import React from "react";
import {Text, View, StyleSheet, Button } from "react-native";
class Home extends React.Component {
    render(){
    return(
        <View style={styles.container}>
        <Text style={styles.title}>Calculator</Text>
        <Button
            title="Computed Marks"
            onPress={() => this.props.navigation.navigate('Computed Marks')}
        />
        </View>
    )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#3AB4BA",
      alignItems: "center",
      justifyContent: "center",
    },
    title:{
      fontWeight: "bold",
      fontSize:50,
      color:"#ffffff",
      marginBottom: 40,
      }
  });
  export default Home; 