# React Native Calculator App
![Test](https://gitlab.com/eunymhoka/eunice_mhoka_rd_frontend/-/raw/main/screenshots/HomePage.png)
![Test](https://gitlab.com/eunymhoka/eunice_mhoka_rd_frontend/-/raw/e37e0655be27b67acb74fa61fa49eae0ffa98eb4/screenshots/ComputedMarks.png)
![Test](https://gitlab.com/eunymhoka/eunice_mhoka_rd_frontend/-/raw/main/screenshots/HistoricalCalculations.png)

## Installation

- git clone https://gitlab.com/eunymhoka/eunice_mhoka_rd_frontend.gitlab
- npm install

## Running
- npx react-native run-android
